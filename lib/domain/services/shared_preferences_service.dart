import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_project_flutter/ui/res/theme/app_theme/app_theme.dart';

class _SharedPreferencesKeys {
  static const theme = 'THEME';
  static const language = 'LANGUAGE';
}

class SharedPreferencesService {
  //Theme
  static Future<AppTheme> getTheme() async {
    final prefs = await SharedPreferences.getInstance();
    String? name = prefs.getString(_SharedPreferencesKeys.theme);
    if (name == null) return appThemes.first;
    return appThemes.firstWhere((theme) => theme.name.name == name);
  }

  static Future<bool> setTheme({required ThemeName name}) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.setString(_SharedPreferencesKeys.theme, name.name);
  }

  static Future<bool> removeTheme() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.remove(_SharedPreferencesKeys.theme);
  }

  //Language
  static Future<bool> setLanguage({required String languageCode}) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.setString(_SharedPreferencesKeys.language, languageCode);
  }

  static Future<String?> getLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    String? languageCode = prefs.getString(_SharedPreferencesKeys.language);
    return languageCode;
  }

  static Future<bool> removeLanguage() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.remove(_SharedPreferencesKeys.language);
  }
}
