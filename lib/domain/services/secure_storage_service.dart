import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class _SecureStorageKeys {
  static const token = 'TOKEN';
}

class SecureStorageService {
  static const _storage = FlutterSecureStorage();

  static Future<String?> getToken() {
    return _storage.read(key: _SecureStorageKeys.token);
  }

  static Future<void> setToken({required String token}) {
    return _storage.write(
      key: _SecureStorageKeys.token,
      value: token,
    );
  }

  static Future<void> removeToken() {
    return _storage.delete(key: _SecureStorageKeys.token);
  }
}
