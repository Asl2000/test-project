import 'dart:async';
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:test_project_flutter/data/models/push/push_notification.dart';
import 'package:test_project_flutter/domain/services/redirect_service.dart';
import 'package:test_project_flutter/ui/res/assets.dart';
import 'package:test_project_flutter/ui/widgets/overlay_push_notification.dart';

class PushNotificationService {
  final RedirectService _redirectService;

  PushNotificationService({
    required RedirectService redirectService,
  }) : _redirectService = redirectService;

  void initPushNotification() {
    FirebaseMessaging.onBackgroundMessage((RemoteMessage message) async {
      final notification = PushNotification.fromRemoteMessage(message);
      _redirect(notification: notification);
      await Firebase.initializeApp();
    });

    //Notification from terminated state
    notificationFromTerminatedState();

    // Notification from background state
    notificationFromBackgroundState();

    // Notification foreground
    notificationForeground();
  }

  void notificationFromTerminatedState() {
    FirebaseMessaging.instance.getInitialMessage().then((message) {
      if (message != null) {
        final notification = PushNotification.fromRemoteMessage(message);
        Timer(const Duration(milliseconds: 2500), () {
          _redirect(notification: notification);
        });
      }
    });
  }

  void notificationFromBackgroundState() {
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      final notification = PushNotification.fromRemoteMessage(message);
      _redirect(notification: notification);
    });
  }

  void notificationForeground() {
    if (Platform.isIOS) {
      FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
        alert: true,
        badge: true,
        sound: true,
      );
    }

    _notificationForeground();
  }

  Future<void> _notificationForeground() async {
    final messaging = FirebaseMessaging.instance;

    final settings = await messaging.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        final notification = PushNotification.fromRemoteMessage(message);
        if (Platform.isAndroid) showListTileNotification(notification: notification);
      });
    }
  }

  void _redirect({required PushNotification notification}) {
    if (notification.type == NotificationType.link) {
      _redirectService.openLink(link: notification.data['link']);
    } else {
      _redirectService.openInformation();
    }
  }

  void showListTileNotification({required PushNotification notification}) {
    showOverlayNotification(
      (BuildContext context) {
        return OverlayPushNotification(
          title: notification.title,
          body: notification.body,
          onOpen: () => _redirect(notification: notification),
          pathIcon: AppIcons.logo,
        );
      },
      duration: const Duration(seconds: 3),
    );
  }
}
