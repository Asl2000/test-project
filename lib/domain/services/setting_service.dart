import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_project_flutter/data/models/language/language.dart';
import 'package:test_project_flutter/domain/services/shared_preferences_service.dart';
import 'package:test_project_flutter/l10n/l10n.dart';
import 'package:test_project_flutter/ui/res/theme/app_theme/app_theme.dart';

class SettingService extends ChangeNotifier {
  late AppTheme _appTheme = appThemes.first;
  late Language _language = L10n.languages.first;

  SettingService() {
    _setFromCashTheme();
    _setFromCashLanguage();
  }

  void _setFromCashTheme() async {
    final theme = await SharedPreferencesService.getTheme();
    _appTheme = theme;
    SystemChrome.setSystemUIOverlayStyle(theme.themeData.appBarTheme.systemOverlayStyle!);
    notifyListeners();
  }

  void _setFromCashLanguage() async {
    String? code = await SharedPreferencesService.getLanguage();
    if (code != null) {
      _language = L10n.languages.where((element) => element.locale.languageCode == code).first;
    }
    notifyListeners();
  }

  Language get language => _language;

  AppTheme get appTheme => _appTheme;

  void setTheme({required AppTheme appTheme}) {
    _appTheme = appTheme;
    SystemChrome.setSystemUIOverlayStyle(appTheme.themeData.appBarTheme.systemOverlayStyle!);
    SharedPreferencesService.setTheme(name: appTheme.name);
    notifyListeners();
  }

  void setLanguage({required Language language}) {
    if (!L10n.languages.contains(language)) return;
    _language = language;
    SharedPreferencesService.setLanguage(languageCode: language.locale.languageCode);
    notifyListeners();
  }

  void switchTheme() {
    if (_appTheme.name == ThemeName.light) {
      setTheme(appTheme: appThemeDark);
    } else {
      setTheme(appTheme: appThemeLight);
    }
  }
}
