import 'package:test_project_flutter/domain/navigator/navigator_service.dart';
import 'package:url_launcher/url_launcher.dart';

class RedirectService {
  final NavigatorService _navigatorService;

  RedirectService({required NavigatorService navigatorService})
      : _navigatorService = navigatorService;

  void openLink({required String link}) async {
    final url = Uri.parse(link);
    await launchUrl(
      url,
      mode: LaunchMode.externalApplication,
    );
  }

  void openInformation() {}
}
