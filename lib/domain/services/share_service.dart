import 'package:share_plus/share_plus.dart';
import 'package:test_project_flutter/data/network/common.dart';

class ShareService {
  static const _host = ApiCommon.apiApp;

  static void user({required int userId}) {
    Share.share('$_host/users/$userId/');
  }
}
