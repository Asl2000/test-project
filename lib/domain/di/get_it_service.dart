import 'package:test_project_flutter/domain/di/injector/injector.dart';
import 'package:test_project_flutter/domain/navigator/navigator_service.dart';

class GetItService {
  NavigatorService get navigatorService => getIt.get<NavigatorService>();

  const GetItService._();
}

const getItService = GetItService._();
