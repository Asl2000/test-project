import 'package:get_it/get_it.dart';
import 'package:redux/redux.dart';
import 'package:test_project_flutter/data/network/api_app.dart';
import 'package:test_project_flutter/data/repositories/post_repository.dart';
import 'package:test_project_flutter/data/repositories/user_repository.dart';
import 'package:test_project_flutter/domain/navigator/navigator_service.dart';
import 'package:test_project_flutter/domain/navigator/router_delegate.dart';
import 'package:test_project_flutter/domain/redux/post/middlewares.dart';
import 'package:test_project_flutter/domain/redux/post/states.dart';
import 'package:test_project_flutter/domain/redux/reducer.dart';
import 'package:test_project_flutter/domain/redux/store.dart';
import 'package:test_project_flutter/domain/redux/user/middlewares.dart';
import 'package:test_project_flutter/domain/redux/user/states.dart';
import 'package:test_project_flutter/domain/services/loger.dart';
import 'package:test_project_flutter/domain/services/push_notification_service.dart';
import 'package:test_project_flutter/domain/services/redirect_service.dart';

class LocatorService {
  final Api api = ApiDio();
  final getIt = GetIt.instance;
  final delegate = routerDelegate;

  late Store<AppState> store;

  late NavigatorService navigatorService;
  late PushNotificationService pushNotificationService;
  late RedirectService redirectService;

  late ApiPostRepository apiPostRepository;
  late ApiUserRepository apiUserRepository;

  LocatorService() {
    initLogger();

    navigatorService = NavigatorService(delegate: routerDelegate);
    redirectService = RedirectService(navigatorService: navigatorService);
    pushNotificationService = PushNotificationService(redirectService: redirectService);

    apiUserRepository = ApiUserRepository(api: api);
    apiPostRepository = ApiPostRepository(api: api);

    store = Store(
      appReducer,
      initialState: AppState(
        userDetailState: UserDetailState(),
        postListState: PostListState(),
      ),
      middleware: [
        UserMiddleware(userRepository: apiUserRepository),
        PostMiddleware(postRepository: apiPostRepository),
      ],
    );

    _register();
  }

  void _register() {
    getIt.registerSingleton<NavigatorService>(navigatorService);
  }
}
