import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_project_flutter/data/models/user/user.dart';
import 'package:test_project_flutter/data/network/exception/exception.dart';

part 'states.freezed.dart';

@freezed
class UserDetailState with _$UserDetailState {
  factory UserDetailState([
    @Default(false) bool isLoading,
    @Default(false) bool isError,
    @Default('') String errorMessage,
    ErrorType? error,
    User? user,
  ]) = _UserDetailState;
}
