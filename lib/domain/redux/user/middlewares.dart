import 'package:redux/redux.dart';
import 'package:test_project_flutter/data/network/exception/exception.dart';
import 'package:test_project_flutter/data/repositories/user_repository.dart';
import 'package:test_project_flutter/domain/redux/store.dart';
import 'package:test_project_flutter/domain/redux/user/actions.dart';

class UserMiddleware implements MiddlewareClass<AppState> {
  final UserRepository _userRepository;

  UserMiddleware({required UserRepository userRepository}) : _userRepository = userRepository;

  @override
  call(store, action, next) {
    // User detail
    if (action is LoadUserDetailAction) {
      Future(() async {
        try {
          final user = await _userRepository.getUser(userId: action.userId);
          store.dispatch(ShowUserDetailAction(user: user));
        } on ServerException catch (e) {
          store.dispatch(ErrorUserDetailAction(
            error: ErrorType.server,
            message: e.message.toString(),
          ));
        }
      });
    }

    next(action);
  }
}
