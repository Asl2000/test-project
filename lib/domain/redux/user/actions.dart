import 'package:test_project_flutter/data/models/user/user.dart';
import 'package:test_project_flutter/data/network/exception/exception.dart';

//User detail

abstract class UserDetailAction {}

class LoadUserDetailAction extends UserDetailAction {
  final int userId;

  LoadUserDetailAction({required this.userId});
}

class ShowUserDetailAction extends UserDetailAction {
  final User? user;

  ShowUserDetailAction({this.user});
}

class ErrorUserDetailAction extends UserDetailAction {
  final ErrorType error;
  final String message;

  ErrorUserDetailAction({
    required this.error,
    required this.message,
  });
}
