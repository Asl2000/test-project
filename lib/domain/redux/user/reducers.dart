import 'package:redux/redux.dart';
import 'package:test_project_flutter/domain/redux/user/actions.dart';
import 'package:test_project_flutter/domain/redux/user/states.dart';

final userDetailReducer = combineReducers<UserDetailState>([
  TypedReducer<UserDetailState, LoadUserDetailAction>(_loadUserDetail),
  TypedReducer<UserDetailState, ErrorUserDetailAction>(_errorUserDetail),
  TypedReducer<UserDetailState, ShowUserDetailAction>(_showUserDetail),
]);

//Self profile

UserDetailState _loadUserDetail(
  UserDetailState state,
  LoadUserDetailAction action,
) =>
    state.copyWith(
      isLoading: true,
      isError: false,
    );

UserDetailState _showUserDetail(
  UserDetailState state,
  ShowUserDetailAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: false,
      user: action.user,
    );

UserDetailState _errorUserDetail(
  UserDetailState state,
  ErrorUserDetailAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: true,
      error: action.error,
      errorMessage: action.message,
    );
