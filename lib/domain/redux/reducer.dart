import 'package:test_project_flutter/domain/redux/post/actions.dart';
import 'package:test_project_flutter/domain/redux/post/reducers.dart';
import 'package:test_project_flutter/domain/redux/store.dart';
import 'package:test_project_flutter/domain/redux/user/actions.dart';
import 'package:test_project_flutter/domain/redux/user/reducers.dart';

AppState appReducer(AppState state, dynamic action) {
  if (action is UserDetailAction) {
    final nextState = userDetailReducer(state.userDetailState, action);
    return state.copyWith(userDetailState: nextState);
  } else if (action is PostListAction) {
    final nextState = postListReducer(state.postListState, action);
    return state.copyWith(postListState: nextState);
  }
  return state;
}
