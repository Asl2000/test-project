// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'store.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AppState {
//User
  UserDetailState get userDetailState =>
      throw _privateConstructorUsedError; //Post
  PostListState get postListState => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AppStateCopyWith<AppState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppStateCopyWith<$Res> {
  factory $AppStateCopyWith(AppState value, $Res Function(AppState) then) =
      _$AppStateCopyWithImpl<$Res, AppState>;
  @useResult
  $Res call({UserDetailState userDetailState, PostListState postListState});

  $UserDetailStateCopyWith<$Res> get userDetailState;
  $PostListStateCopyWith<$Res> get postListState;
}

/// @nodoc
class _$AppStateCopyWithImpl<$Res, $Val extends AppState>
    implements $AppStateCopyWith<$Res> {
  _$AppStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userDetailState = null,
    Object? postListState = null,
  }) {
    return _then(_value.copyWith(
      userDetailState: null == userDetailState
          ? _value.userDetailState
          : userDetailState // ignore: cast_nullable_to_non_nullable
              as UserDetailState,
      postListState: null == postListState
          ? _value.postListState
          : postListState // ignore: cast_nullable_to_non_nullable
              as PostListState,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $UserDetailStateCopyWith<$Res> get userDetailState {
    return $UserDetailStateCopyWith<$Res>(_value.userDetailState, (value) {
      return _then(_value.copyWith(userDetailState: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $PostListStateCopyWith<$Res> get postListState {
    return $PostListStateCopyWith<$Res>(_value.postListState, (value) {
      return _then(_value.copyWith(postListState: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_AppStateCopyWith<$Res> implements $AppStateCopyWith<$Res> {
  factory _$$_AppStateCopyWith(
          _$_AppState value, $Res Function(_$_AppState) then) =
      __$$_AppStateCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({UserDetailState userDetailState, PostListState postListState});

  @override
  $UserDetailStateCopyWith<$Res> get userDetailState;
  @override
  $PostListStateCopyWith<$Res> get postListState;
}

/// @nodoc
class __$$_AppStateCopyWithImpl<$Res>
    extends _$AppStateCopyWithImpl<$Res, _$_AppState>
    implements _$$_AppStateCopyWith<$Res> {
  __$$_AppStateCopyWithImpl(
      _$_AppState _value, $Res Function(_$_AppState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userDetailState = null,
    Object? postListState = null,
  }) {
    return _then(_$_AppState(
      userDetailState: null == userDetailState
          ? _value.userDetailState
          : userDetailState // ignore: cast_nullable_to_non_nullable
              as UserDetailState,
      postListState: null == postListState
          ? _value.postListState
          : postListState // ignore: cast_nullable_to_non_nullable
              as PostListState,
    ));
  }
}

/// @nodoc

class _$_AppState implements _AppState {
  const _$_AppState(
      {required this.userDetailState, required this.postListState});

//User
  @override
  final UserDetailState userDetailState;
//Post
  @override
  final PostListState postListState;

  @override
  String toString() {
    return 'AppState(userDetailState: $userDetailState, postListState: $postListState)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AppState &&
            (identical(other.userDetailState, userDetailState) ||
                other.userDetailState == userDetailState) &&
            (identical(other.postListState, postListState) ||
                other.postListState == postListState));
  }

  @override
  int get hashCode => Object.hash(runtimeType, userDetailState, postListState);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AppStateCopyWith<_$_AppState> get copyWith =>
      __$$_AppStateCopyWithImpl<_$_AppState>(this, _$identity);
}

abstract class _AppState implements AppState {
  const factory _AppState(
      {required final UserDetailState userDetailState,
      required final PostListState postListState}) = _$_AppState;

  @override //User
  UserDetailState get userDetailState;
  @override //Post
  PostListState get postListState;
  @override
  @JsonKey(ignore: true)
  _$$_AppStateCopyWith<_$_AppState> get copyWith =>
      throw _privateConstructorUsedError;
}
