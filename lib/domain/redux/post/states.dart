import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_project_flutter/data/models/post/post.dart';
import 'package:test_project_flutter/data/network/exception/exception.dart';

part 'states.freezed.dart';

@freezed
class PostListState with _$PostListState {
  factory PostListState([
    @Default(false) bool isLoading,
    @Default(false) bool isError,
    @Default('') String errorMessage,
    ErrorType? error,
    @Default([]) List<Post> posts,
  ]) = _PostListState;
}
