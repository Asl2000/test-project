import 'package:test_project_flutter/data/models/post/post.dart';
import 'package:test_project_flutter/data/network/exception/exception.dart';

//List posts

abstract class PostListAction {}

class LoadPostListAction extends PostListAction {}

class ShowPostListAction extends PostListAction {
  final List<Post> posts;

  ShowPostListAction({required this.posts});
}

class ErrorPostListAction extends PostListAction {
  final ErrorType error;
  final String message;

  ErrorPostListAction({
    required this.error,
    required this.message,
  });
}
