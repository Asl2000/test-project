import 'package:redux/redux.dart';
import 'package:test_project_flutter/data/network/exception/exception.dart';
import 'package:test_project_flutter/data/repositories/post_repository.dart';
import 'package:test_project_flutter/domain/redux/post/actions.dart';
import 'package:test_project_flutter/domain/redux/store.dart';

class PostMiddleware implements MiddlewareClass<AppState> {
  final PostRepository _postRepository;

  PostMiddleware({required PostRepository postRepository}) : _postRepository = postRepository;

  @override
  call(store, action, next)  {
    // List post
    if (action is LoadPostListAction) {
      Future(()async{
        try {
          final posts = await _postRepository.getListPost();
          store.dispatch(ShowPostListAction(posts: posts));
        } on ServerException catch (e) {
          store.dispatch(ErrorPostListAction(
            error: ErrorType.server,
            message: e.message.toString(),
          ));
        }
      });
    }

    next(action);
  }
}
