import 'package:redux/redux.dart';
import 'package:test_project_flutter/domain/redux/post/actions.dart';
import 'package:test_project_flutter/domain/redux/post/states.dart';

final postListReducer = combineReducers<PostListState>([
  TypedReducer<PostListState, LoadPostListAction>(_loadPostList),
  TypedReducer<PostListState, ErrorPostListAction>(_errorPostList),
  TypedReducer<PostListState, ShowPostListAction>(_showPostList),
]);

//List cities

PostListState _loadPostList(
  PostListState state,
  LoadPostListAction action,
) =>
    state.copyWith(
      isLoading: true,
      isError: false,
    );

PostListState _showPostList(
  PostListState state,
  ShowPostListAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: false,
      posts: action.posts,
    );

PostListState _errorPostList(
  PostListState state,
  ErrorPostListAction action,
) =>
    state.copyWith(
      isLoading: false,
      isError: true,
      error: action.error,
      errorMessage: action.message,
    );
