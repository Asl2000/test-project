import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_project_flutter/domain/redux/post/states.dart';
import 'package:test_project_flutter/domain/redux/user/states.dart';

part 'store.freezed.dart';

@freezed
class AppState with _$AppState {
  const factory AppState({
    //User
    required UserDetailState userDetailState,
    //Post
    required PostListState postListState,
  }) = _AppState;
}
