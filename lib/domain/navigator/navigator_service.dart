import 'package:beamer/beamer.dart';
import 'package:test_project_flutter/domain/navigator/pages.dart';

class NavigatorService {
  final BeamerDelegate _delegate;

  NavigatorService({required BeamerDelegate delegate}) : _delegate = delegate;

  bool canPop() => _delegate.canPopBeamLocation;

  void onPop() => _delegate.popBeamLocation();

  void onListPost() => _delegate.beamToReplacementNamed(RouterPath.postList);

  void onUser({required int userId}) {
    _delegate.beamToNamed(RouterPath.userDetailWithId(userId: userId));
  }
}
