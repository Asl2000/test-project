import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:test_project_flutter/domain/navigator/pages.dart';
import 'package:test_project_flutter/ui/screens/post/post_list_screen.dart';
import 'package:test_project_flutter/ui/screens/splash/spash_screen.dart';
import 'package:test_project_flutter/ui/screens/user/user_detail_screen.dart';

final beamerLocationBuilder = BeamerLocationBuilder(
  beamLocations: [
    _SplashLocation(),
    _PostListLocation(),
    _UserDetailLocation(),
  ],
);

class _PostListLocation extends BeamLocation<BeamState> {
  @override
  List<Pattern> get pathPatterns => [RouterPath.postList];

  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) {
    return [
      const BeamPage(
        key: ValueKey(RouterPath.postList),
        child: PostListScreen(),
      ),
    ];
  }
}

class _UserDetailLocation extends BeamLocation<BeamState> {
  @override
  List<Pattern> get pathPatterns => [RouterPath.userDetail];

  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) {
    final userId = int.parse(state.pathParameters['userId']!);
    return [
      BeamPage(
        key: ValueKey(RouterPath.userDetailWithId(userId: userId)),
        child: UserDetailScreen(userId: userId),
      ),
    ];
  }
}

class _SplashLocation extends BeamLocation<BeamState> {
  @override
  List<Pattern> get pathPatterns => [RouterPath.splash];

  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) {
    return [
      const BeamPage(
        key: ValueKey(RouterPath.splash),
        child: SplashScreen(),
      ),
    ];
  }
}
