import 'package:beamer/beamer.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:test_project_flutter/domain/navigator/locations.dart';
import 'package:test_project_flutter/domain/navigator/pages.dart';

final routerDelegate = BeamerDelegate(
  initialPath: RouterPath.splash,
  locationBuilder: beamerLocationBuilder,
);

final routerParser = RouteBeamerParser();

class RouteBeamerParser extends RouteInformationParser<RouteInformation> {
  RouteBeamerParser({this.onParse});

  final RouteInformation Function(RouteInformation)? onParse;

  @override
  SynchronousFuture<RouteInformation> parseRouteInformation(RouteInformation routeInformation) {
    late String? location = routeInformation.location;
    if (location?[0] != '/') location = '/${routeInformation.location}';
    final information = RouteInformation(
      state: routeInformation.state,
      location: location,
    );
    return SynchronousFuture(onParse?.call(information) ?? information);
  }

  @override
  RouteInformation restoreRouteInformation(RouteInformation configuration) => configuration;
}
