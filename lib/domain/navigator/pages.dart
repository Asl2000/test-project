class RouterPath {
  static const String splash = '/splash/';
  static const String postList = '/posts/';

  static String userDetailWithId({required int userId}) => '/user/$userId/';
  static const String userDetail = '/user/:userId/';
}

class RouterArguments {}
