import 'dart:async';

import 'package:beamer/beamer.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';
import 'package:test_project_flutter/domain/di/locator_service.dart';
import 'package:test_project_flutter/domain/navigator/router_delegate.dart';
import 'package:test_project_flutter/domain/redux/store.dart';
import 'package:test_project_flutter/domain/services/setting_service.dart';
import 'package:test_project_flutter/l10n/l10n.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:test_project_flutter/ui/screens/splash/error_screen.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:test_project_flutter/ui/widgets/loading_widget.dart';

import 'firebase_options.dart';

void main() async {
  runZonedGuarded<Future<void>>(() async {
    WidgetsFlutterBinding.ensureInitialized();
    await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);

    if (kReleaseMode) {
      ErrorWidget.builder = (FlutterErrorDetails details) => const ErrorScreen();
      FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterFatalError;
    }

    final locator = LocatorService();

    runApp(FApp(locator: locator));
  }, (error, stack) {
    if (kReleaseMode) {
      FirebaseCrashlytics.instance.recordError(error, stack, fatal: true);
    }
  });
}

class FApp extends StatelessWidget {
  final LocatorService locator;
  final _initialization = Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);

  FApp({
    Key? key,
    required this.locator,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _initialization,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return MaterialApp(
            home: Scaffold(
              body: Center(
                child: kReleaseMode
                    ? const ErrorScreen()
                    : Text(
                        'Error: ${snapshot.error}',
                        style: const TextStyle(color: Colors.red),
                      ),
              ),
            ),
          );
        }

        if (snapshot.connectionState == ConnectionState.done) return App(locator: locator);

        return const MaterialApp(
          home: Scaffold(
            body: LoadingWidget(),
          ),
        );
      },
    );
  }
}

class App extends StatefulWidget {
  final LocatorService locator;

  const App({
    Key? key,
    required this.locator,
  }) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  void initState() {
    super.initState();
    final pushNotificationService = widget.locator.pushNotificationService;
    pushNotificationService.initPushNotification();
  }

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: widget.locator.store,
      child: ChangeNotifierProvider<SettingService>(
        create: (context) => SettingService(),
        builder: (context, child) {
          final settings = Provider.of<SettingService>(context);

          return OverlaySupport.global(
            child: MaterialApp.router(
              title: 'Test',
              theme: settings.appTheme.themeData,
              locale: settings.language.locale,
              localizationsDelegates: AppLocalizations.localizationsDelegates,
              supportedLocales: L10n.languages.map((e) => e.locale).toList(),
              routerDelegate: widget.locator.delegate,
              routeInformationParser: routerParser,
              backButtonDispatcher: BeamerBackButtonDispatcher(delegate: widget.locator.delegate),
              debugShowCheckedModeBanner: false,
            ),
          );
        },
      ),
    );
  }
}
