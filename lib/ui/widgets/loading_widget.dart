import 'package:flutter/material.dart';
import 'package:test_project_flutter/ui/res/assets.dart';

class LoadingWidget extends StatefulWidget {
  final AlignmentGeometry? alignment;

  const LoadingWidget({
    Key? key,
    this.alignment,
  }) : super(key: key);

  @override
  State<LoadingWidget> createState() => _LoadingWidgetState();
}

class _LoadingWidgetState extends State<LoadingWidget> with SingleTickerProviderStateMixin {
  late AnimationController? controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 900),
    );
    controller!.repeat();
  }

  @override
  dispose() {
    controller!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: widget.alignment ?? Alignment.center,
      height: MediaQuery.of(context).size.height / 1.25,
      child: RotationTransition(
        turns: controller as AnimationController,
        child: Image.asset(AppImages.loading),
      ),
    );
  }
}
