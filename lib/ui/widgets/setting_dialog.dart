import 'package:blur/blur.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:test_project_flutter/domain/services/setting_service.dart';
import 'package:test_project_flutter/l10n/l10n.dart';
import 'package:test_project_flutter/ui/res/theme/app_theme/app_theme.dart';

class SettingDialog extends StatelessWidget {
  const SettingDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Blur(
          blur: 5,
          blurColor: Theme.of(context).scaffoldBackgroundColor,
          overlay: Container(
            padding: const EdgeInsets.only(bottom: 30, top: 10),
            color: Colors.transparent,
            child: const Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                _ThemeButton(),
                SizedBox(height: 15),
                _LanguageButton(),
              ],
            ),
          ),
          child: Container(height: MediaQuery.of(context).size.height * 0.3),
        ),
      ],
    );
  }
}

class _ThemeButton extends StatefulWidget {
  const _ThemeButton({Key? key}) : super(key: key);

  @override
  State<_ThemeButton> createState() => _ThemeButtonState();
}

class _ThemeButtonState extends State<_ThemeButton> {
  late SettingService settingService;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    settingService = Provider.of<SettingService>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CupertinoSwitch(
            thumbColor:
                settingService.appTheme.name == ThemeName.light ? Colors.black : Colors.white,
            activeColor: Theme.of(context).disabledColor,
            value: settingService.appTheme.name == ThemeName.light,
            onChanged: (val) => settingService.switchTheme(),
          ),
          const SizedBox(width: 10),
          Text(context.l10n.theme),
        ],
      ),
    );
  }
}

class _LanguageButton extends StatefulWidget {
  const _LanguageButton({Key? key}) : super(key: key);

  @override
  State<_LanguageButton> createState() => _LanguageButtonState();
}

class _LanguageButtonState extends State<_LanguageButton> {
  late SettingService settingService;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    settingService = Provider.of<SettingService>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        for (var language in L10n.languages) ...[
          GestureDetector(
            onTap: () => settingService.setLanguage(language: language),
            child: Container(
              padding: const EdgeInsets.symmetric(
                vertical: 5,
                horizontal: 16,
              ),
              decoration: BoxDecoration(
                color: Colors.transparent,
                border: Border.all(
                  color: settingService.language.locale.languageCode == language.locale.languageCode
                      ? Theme.of(context).colorScheme.primary
                      : Colors.transparent,
                  width: 1,
                ),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SvgPicture.asset(
                    language.flag,
                    width: 50,
                  ),
                  const SizedBox(width: 10),
                  Text(language.name),
                ],
              ),
            ),
          ),
          const SizedBox(height: 10),
        ],
      ],
    );
  }
}
