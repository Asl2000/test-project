import 'package:flutter/material.dart';

class ErrorState extends StatelessWidget {
  final String text;

  const ErrorState({
    Key? key,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          SizedBox(height: MediaQuery.of(context).size.height * .3),
          Text(
            text,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: MediaQuery.of(context).size.height * .3),
        ],
      ),
    );
  }
}
