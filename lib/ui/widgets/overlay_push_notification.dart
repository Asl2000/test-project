import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OverlayPushNotification extends StatelessWidget {
  final Function()? onOpen;
  final String title;
  final String body;
  final String? pathIcon;
  final Color? color;
  final Widget? icon;

  const OverlayPushNotification({
    Key? key,
    required this.title,
    required this.body,
    this.onOpen,
    this.pathIcon,
    this.color,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      elevation: 16,
      child: SafeArea(
        bottom: false,
        child: GestureDetector(
          onTap: onOpen,
          child: ListTile(
            title: Text(title),
            subtitle: Text(body),
            leading: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                color: color ?? Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(6),
              ),
              alignment: Alignment.center,
              child: icon ??
                  SvgPicture.asset(
                    pathIcon!,
                    allowDrawingOutsideViewBox: true,
                    color: Colors.white,
                  ),
            ),
          ),
        ),
      ),
    );
  }
}
