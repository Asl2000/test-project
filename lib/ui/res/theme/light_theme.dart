import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_project_flutter/ui/res/colors.dart';
import 'package:test_project_flutter/ui/res/text_style.dart';

ThemeData lightThemeData = ThemeData(
  primaryColor: AppColoredColors.primary,
  disabledColor: lightAppColors.disabledColor,
  scaffoldBackgroundColor: lightAppColors.backgroundColor,
  appBarTheme: AppBarTheme(
    elevation: 0.0,
    titleTextStyle: displayMedium.copyWith(color: lightAppColors.labelPrimary),
    backgroundColor: lightAppColors.backgroundColor,
    systemOverlayStyle: SystemUiOverlayStyle.dark,
    iconTheme: IconThemeData(color: lightAppColors.labelPrimary),
  ),
  textTheme: TextTheme(
    displayLarge: displayLarge.copyWith(color: lightAppColors.labelPrimary),
    displayMedium: displayMedium.copyWith(color: lightAppColors.labelPrimary),
    displaySmall: displaySmall.copyWith(color: lightAppColors.labelPrimary),
    headlineMedium: headlineMedium.copyWith(color: lightAppColors.labelPrimary),
    headlineSmall: headlineSmall.copyWith(color: lightAppColors.labelPrimary),
    titleLarge: titleLarge.copyWith(color: lightAppColors.labelPrimary),
    titleMedium: titleMedium.copyWith(color: lightAppColors.labelPrimary),
    titleSmall: titleSmall.copyWith(color: lightAppColors.labelPrimary),
    bodyLarge: bodyLarge.copyWith(color: lightAppColors.labelPrimary),
    bodyMedium: bodyMedium.copyWith(color: lightAppColors.labelPrimary),
    labelLarge: labelLarge.copyWith(color: lightAppColors.labelPrimary),
    bodySmall: bodySmall.copyWith(color: lightAppColors.labelPrimary),
    labelSmall: labelSmall.copyWith(color: lightAppColors.labelPrimary),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      elevation: MaterialStateProperty.resolveWith<double>((Set<MaterialState> states) => 0.0),
      textStyle:
          MaterialStateProperty.resolveWith<TextStyle>((Set<MaterialState> states) => bodyLarge),
      shape: MaterialStateProperty.resolveWith<OutlinedBorder>(
        (Set<MaterialState> states) => RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
      ),
      padding: MaterialStateProperty.resolveWith<EdgeInsetsGeometry>(
        (Set<MaterialState> states) => const EdgeInsets.symmetric(vertical: 15.0, horizontal: 39),
      ),
      backgroundColor: MaterialStateProperty.resolveWith<Color>(
        (Set<MaterialState> states) {
          if (states.contains(MaterialState.disabled)) {
            return AppColoredColors.primary.withOpacity(0.5);
          }
          return AppColoredColors.primary;
        },
      ),
    ),
  ),
  colorScheme: ColorScheme.light(
    primary: AppColoredColors.primary,
    secondary: lightAppColors.secondBackgroundColor,
  ),
);
