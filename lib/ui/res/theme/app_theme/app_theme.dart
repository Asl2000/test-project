import 'package:flutter/material.dart';
import 'package:test_project_flutter/ui/res/colors.dart';
import 'package:test_project_flutter/ui/res/theme/dark_theme.dart';
import 'package:test_project_flutter/ui/res/theme/light_theme.dart';

class AppTheme {
  final ThemeName name;
  final ThemeData themeData;
  final AppColors colors;

  AppTheme({
    required this.name,
    required this.themeData,
    required this.colors,
  });
}

enum ThemeName {
  light,
  dark,
}

final appThemeLight = AppTheme(
  name: ThemeName.light,
  themeData: lightThemeData,
  colors: lightAppColors,
);

final appThemeDark = AppTheme(
  name: ThemeName.dark,
  themeData: darkThemeData,
  colors: darkAppColors,
);

final appThemes = [
  appThemeLight,
  appThemeDark,
];
