import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_project_flutter/ui/res/colors.dart';
import 'package:test_project_flutter/ui/res/text_style.dart';

ThemeData darkThemeData = ThemeData(
  primaryColor: AppColoredColors.primary,
  disabledColor: darkAppColors.disabledColor,
  scaffoldBackgroundColor: darkAppColors.backgroundColor,
  appBarTheme: AppBarTheme(
    elevation: 0.0,
    titleTextStyle: displayMedium.copyWith(color: darkAppColors.labelPrimary),
    backgroundColor: darkAppColors.backgroundColor,
    systemOverlayStyle: SystemUiOverlayStyle.light,
    iconTheme: IconThemeData(color: darkAppColors.labelPrimary),
  ),
  textTheme: TextTheme(
    displayLarge: displayLarge.copyWith(color: darkAppColors.labelPrimary),
    displayMedium: displayMedium.copyWith(color: darkAppColors.labelPrimary),
    displaySmall: displaySmall.copyWith(color: darkAppColors.labelPrimary),
    headlineMedium: headlineMedium.copyWith(color: darkAppColors.labelPrimary),
    headlineSmall: headlineSmall.copyWith(color: darkAppColors.labelPrimary),
    titleLarge: titleLarge.copyWith(color: darkAppColors.labelPrimary),
    titleMedium: titleMedium.copyWith(color: darkAppColors.labelPrimary),
    titleSmall: titleSmall.copyWith(color: darkAppColors.labelPrimary),
    bodyLarge: bodyLarge.copyWith(color: darkAppColors.labelPrimary),
    bodyMedium: bodyMedium.copyWith(color: darkAppColors.labelPrimary),
    labelLarge: labelLarge.copyWith(color: darkAppColors.labelPrimary),
    bodySmall: bodySmall.copyWith(color: darkAppColors.labelPrimary),
    labelSmall: labelSmall.copyWith(color: darkAppColors.labelPrimary),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      elevation: MaterialStateProperty.resolveWith<double>((Set<MaterialState> states) => 0.0),
      textStyle:
          MaterialStateProperty.resolveWith<TextStyle>((Set<MaterialState> states) => bodyLarge),
      shape: MaterialStateProperty.resolveWith<OutlinedBorder>(
        (Set<MaterialState> states) => RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
      ),
      padding: MaterialStateProperty.resolveWith<EdgeInsetsGeometry>(
        (Set<MaterialState> states) => const EdgeInsets.symmetric(vertical: 15.0, horizontal: 39),
      ),
      backgroundColor: MaterialStateProperty.resolveWith<Color>(
        (Set<MaterialState> states) {
          if (states.contains(MaterialState.disabled)) {
            return AppColoredColors.primary.withOpacity(0.5);
          }
          return AppColoredColors.primary;
        },
      ),
    ),
  ),
  colorScheme: ColorScheme.light(
    primary: AppColoredColors.primary,
    secondary: darkAppColors.secondBackgroundColor,
  ),
);
