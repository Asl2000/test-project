import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

String convertDate({
  required DateTime? date,
  bool withTime = false,
  Locale? monthLocale,
}) {
  if (date != null) {
    if (withTime) {
      if (monthLocale == null) return DateFormat('dd.MM.yyyy hh:mm').format(date);
      return DateFormat('dd MMMM yyyy hh:mm', monthLocale.languageCode).format(date);
    } else {
      if (monthLocale == null) return DateFormat('dd.MM.yyyy').format(date);
      return DateFormat('dd MMMM yyyy', monthLocale.languageCode).format(date);
    }
  } else {
    return '';
  }
}

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}
