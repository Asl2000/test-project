import 'package:flutter/material.dart';

class AppColors {
  final Color backgroundColor;
  final Color bottomNavigationBar;
  final Color secondBackgroundColor;
  final Color labelPrimary;
  final Color labelTertiary;
  final Color disabledColor;

  AppColors({
    required this.backgroundColor,
    required this.secondBackgroundColor,
    required this.bottomNavigationBar,
    required this.labelPrimary,
    required this.labelTertiary,
    required this.disabledColor,
  });
}

final lightAppColors = AppColors(
  backgroundColor: const Color(0XFFF1F2F6),
  secondBackgroundColor: Colors.white,
  bottomNavigationBar: const Color(0XFF242F58),
  labelPrimary: Colors.black,
  labelTertiary: const Color(0XFF55596A),
  disabledColor:  Colors.grey,
);

final darkAppColors = AppColors(
  backgroundColor: const Color(0XFF161618),
  bottomNavigationBar: const Color(0XFF252528),
  secondBackgroundColor: const Color(0XFF252528),
  labelPrimary: Colors.white,
  labelTertiary: const Color(0XFFE9E9E9),
  disabledColor:  Colors.white.withOpacity(.5),
);

class AppColoredColors {
  static const error = Color(0XFFFF0000);
  static const primary = Colors.orange;
  static const link = Colors.blue;
}
