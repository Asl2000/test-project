class AppIcons {
  static const logo = 'assets/logos/logo.svg';
  static const notConnection = 'assets/icons/not-connection.svg';
  static const russiaFlag = 'assets/icons/russia-flag.svg';
  static const usaFlag = 'assets/icons/usa-flag.svg';
}

class AppImages {
  static const loading = 'assets/images/loading.png';
}
