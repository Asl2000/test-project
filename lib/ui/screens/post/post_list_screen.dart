import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:test_project_flutter/domain/redux/post/actions.dart';
import 'package:test_project_flutter/domain/redux/post/states.dart';
import 'package:test_project_flutter/domain/redux/store.dart';
import 'package:test_project_flutter/l10n/l10n.dart';
import 'package:test_project_flutter/ui/widgets/error_widget.dart';
import 'package:test_project_flutter/ui/widgets/loading_widget.dart';
import 'package:test_project_flutter/ui/screens/post/widget/post_card.dart';
import 'package:test_project_flutter/ui/widgets/setting_dialog.dart';

class PostListScreen extends StatefulWidget {
  const PostListScreen({Key? key}) : super(key: key);

  @override
  State<PostListScreen> createState() => _PostListScreenState();
}

class _PostListScreenState extends State<PostListScreen> {
  late Store<AppState> _store;

  @override
  void initState() {
    super.initState();
    _store = StoreProvider.of<AppState>(context, listen: false);
    if (_store.state.postListState.posts.isEmpty) onLoadPosts();
  }

  void onLoadPosts() => _store.dispatch(LoadPostListAction());

  void openSetting() {
    showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
      context: context,
      builder: (BuildContext context) => const SettingDialog(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () => Future(onLoadPosts),
        child: CustomScrollView(
          shrinkWrap: true,
          physics: const AlwaysScrollableScrollPhysics(),
          slivers: [
            CupertinoSliverNavigationBar(
              border: Border.all(width: 0, color: Colors.transparent),
              backgroundColor: Theme.of(context).appBarTheme.backgroundColor!.withOpacity(.5),
              trailing: Material(
                color: Colors.transparent,
                child: IconButton(
                  onPressed: openSetting,
                  icon: Icon(
                    Icons.more_vert,
                    color: Theme.of(context).textTheme.titleLarge!.color,
                  ),
                ),
              ),
              alwaysShowMiddle: false,
              middle: Text(
                context.l10n.posts,
                style: Theme.of(context).textTheme.titleLarge,
              ),
              largeTitle: Text(
                context.l10n.posts,
                style: TextStyle(color: Theme.of(context).textTheme.titleLarge!.color),
              ),
            ),
            SliverList(delegate: SliverChildListDelegate([const _Body()]))
          ],
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, PostListState>(
      converter: (store) => store.state.postListState,
      builder: (BuildContext context, PostListState state) {
        if (state.isLoading) return const LoadingWidget();
        if (state.isError) return ErrorState(text: state.errorMessage);
        if (state.posts.isEmpty) return ErrorState(text: context.l10n.posts_empty);

        return Container(
          margin: const EdgeInsets.only(top: 20),
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: [
              for (var post in state.posts) PostCard(post: post),
            ],
          ),
        );
      },
    );
  }
}
