import 'package:flutter/material.dart';
import 'package:test_project_flutter/data/models/post/post.dart';
import 'package:test_project_flutter/domain/di/get_it_service.dart';
import 'package:test_project_flutter/ui/widgets/shadow_container_widget.dart';

class PostCard extends StatelessWidget {
  final Post post;

  const PostCard({
    Key? key,
    required this.post,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => getItService.navigatorService.onUser(userId: post.userId),
      child: ShadowContainer(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(child: Text(post.title)),
            const SizedBox(width: 10),
            Icon(
              Icons.arrow_forward_ios_rounded,
              color: Theme.of(context).colorScheme.primary,
              size: 20,
            )
          ],
        ),
      ),
    );
  }
}
