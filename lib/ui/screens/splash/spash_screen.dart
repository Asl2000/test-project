// ignore_for_file: constant_identifier_names

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:test_project_flutter/domain/di/get_it_service.dart';
import 'package:test_project_flutter/ui/res/assets.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  static const TIMEOUT_SECONDS = 2;
  final navigatorService = getItService.navigatorService;

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  startTimer() {
    return Timer(
      const Duration(seconds: TIMEOUT_SECONDS),
      navigatorService.onListPost,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SvgPicture.asset(
          AppIcons.logo,
          width: MediaQuery.of(context).size.width * .6,
        ),
      ),
    );
  }
}
