import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:test_project_flutter/domain/di/get_it_service.dart';
import 'package:test_project_flutter/l10n/l10n.dart';
import 'package:test_project_flutter/ui/res/assets.dart';

class ErrorScreen extends StatelessWidget {
  const ErrorScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(
                AppIcons.notConnection,
                color: Theme.of(context).colorScheme.primary,
                width: 60,
              ),
              const SizedBox(height: 30),
              Text(
                context.l10n.dear_users,
                style: Theme.of(context).textTheme.titleLarge,
              ),
              const SizedBox(height: 5),
              Text(
                context.l10n.text_error_screen,
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 30),
              ElevatedButton(
                onPressed: getItService.navigatorService.onListPost,
                child: Text(context.l10n.go_to_main),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
