import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:test_project_flutter/domain/di/get_it_service.dart';
import 'package:test_project_flutter/domain/navigator/navigator_service.dart';
import 'package:test_project_flutter/domain/redux/store.dart';
import 'package:test_project_flutter/domain/redux/user/actions.dart';
import 'package:test_project_flutter/domain/redux/user/states.dart';
import 'package:test_project_flutter/domain/services/share_service.dart';
import 'package:test_project_flutter/l10n/l10n.dart';
import 'package:test_project_flutter/ui/res/colors.dart';
import 'package:test_project_flutter/ui/widgets/error_widget.dart';
import 'package:test_project_flutter/ui/widgets/loading_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class UserDetailScreen extends StatefulWidget {
  final int userId;

  const UserDetailScreen({
    Key? key,
    required this.userId,
  }) : super(key: key);

  @override
  State<UserDetailScreen> createState() => _UserDetailScreenState();
}

class _UserDetailScreenState extends State<UserDetailScreen> {
  late Store<AppState> _store;
  late NavigatorService navigatorService;

  @override
  void initState() {
    super.initState();
    navigatorService = getItService.navigatorService;
    _store = StoreProvider.of<AppState>(context, listen: false);
    if (_store.state.userDetailState.user?.id != widget.userId) onLoadUser();
  }

  void onLoadUser() => _store.dispatch(LoadUserDetailAction(userId: widget.userId));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () => Future(onLoadUser),
        child: CustomScrollView(
          shrinkWrap: true,
          physics: const AlwaysScrollableScrollPhysics(),
          slivers: [
            CupertinoSliverNavigationBar(
              border: Border.all(width: 0, color: Colors.transparent),
              backgroundColor: Theme.of(context).appBarTheme.backgroundColor!.withOpacity(.5),
              alwaysShowMiddle: false,
              leading: Material(
                color: Colors.transparent,
                child: IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Theme.of(context).textTheme.titleLarge!.color,
                  ),
                  onPressed: navigatorService.onPop,
                ),
              ),
              trailing: Material(
                color: Colors.transparent,
                child: IconButton(
                  onPressed: () => ShareService.user(userId: widget.userId),
                  icon: Icon(
                    Icons.share,
                    color: Theme.of(context).textTheme.titleLarge!.color,
                  ),
                ),
              ),
              middle: _Title(style: Theme.of(context).textTheme.titleLarge),
              largeTitle: _Title(
                style: TextStyle(color: Theme.of(context).textTheme.titleLarge!.color),
              ),
            ),
            SliverList(delegate: SliverChildListDelegate([const _Body()]))
          ],
        ),
      ),
    );
  }
}

class _Title extends StatelessWidget {
  final TextStyle? style;

  const _Title({
    Key? key,
    this.style,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, UserDetailState>(
      converter: (store) => store.state.userDetailState,
      builder: (BuildContext context, UserDetailState state) {
        if (state.isLoading || state.isError) return Text('...', style: style);
        return Text(state.user!.name, style: style);
      },
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, UserDetailState>(
      converter: (store) => store.state.userDetailState,
      builder: (BuildContext context, UserDetailState state) {
        if (state.isLoading) return const LoadingWidget();
        if (state.isError) return ErrorState(text: state.errorMessage);

        return Container(
          margin: const EdgeInsets.only(top: 20),
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                context.l10n.contact,
                style: Theme.of(context).textTheme.titleLarge,
              ),
              Text('${state.user!.username} ${state.user!.email}'),
              _TextLink(
                link: 'tel:${state.user!.phone}',
                text: state.user!.phone,
              ),
              const SizedBox(height: 20),
              Text(
                context.l10n.address,
                style: Theme.of(context).textTheme.titleLarge,
              ),
              Text(state.user!.address.city),
              Text('${state.user!.address.street} ${state.user!.address.suite}'),
              Text(state.user!.address.zipcode),
              const SizedBox(height: 20),
              Text(
                context.l10n.company,
                style: Theme.of(context).textTheme.titleLarge,
              ),
              Text(state.user!.company.name),
              Text(state.user!.company.catchPhrase),
              Text(state.user!.company.bs),
              const SizedBox(height: 20),
              _TextLink(text: state.user!.website),
            ],
          ),
        );
      },
    );
  }
}

class _TextLink extends StatelessWidget {
  final String? link;
  final String text;

  const _TextLink({
    Key? key,
    this.link,
    required this.text,
  }) : super(key: key);

  void onTapLink() {
    final url = Uri.parse(link ?? text);
    launchUrl(
      url,
      mode: LaunchMode.externalApplication,
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTapLink,
      child: Text(
        text,
        style: const TextStyle(color: AppColoredColors.link),
      ),
    );
  }
}
