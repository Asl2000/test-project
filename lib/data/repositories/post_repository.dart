import 'package:test_project_flutter/data/models/post/post.dart';
import 'package:test_project_flutter/data/network/api_app.dart';

abstract class PostRepository {
  Future<List<Post>> getListPost();
}

class ApiPostRepository implements PostRepository {
  final Api _api;

  ApiPostRepository({required Api api}) : _api = api;

  @override
  Future<List<Post>> getListPost() => _api.getListPost();
}
