import 'package:test_project_flutter/data/models/user/user.dart';
import 'package:test_project_flutter/data/network/api_app.dart';

abstract class UserRepository {
  Future<User> getUser({required int userId});
}

class ApiUserRepository implements UserRepository {
  final Api _api;

  ApiUserRepository({required Api api}) : _api = api;

  @override
  Future<User> getUser({required int userId}) => _api.getUser(userId: userId);
}
