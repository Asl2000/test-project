enum ErrorType {
  server,
  notFound,
}

class ServerException implements Exception {
  final String message;

  ServerException({required this.message});
}
