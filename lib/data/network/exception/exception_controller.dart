import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:test_project_flutter/ui/res/colors.dart';

class ExceptionController {

  static void processDioError(DioException dioError) {
    bool isShow = true;
    String message = '';

    try {
      final Map errorMap = dioError.response!.data as Map;
      for (var key in errorMap.keys) {
        message = '$key: ${errorMap[key]}';
      }
    } catch (e) {
      message = dioError.message ?? 'Error server';
    }

    if (isShow) showError(message: message);
  }

  static void showError({required String message}) {
    showOverlayNotification(
      (BuildContext context) {
        return Material(
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(
              vertical: 10,
              horizontal: 30,
            ),
            color: AppColoredColors.error,
            child: Text(
              message,
              style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                    color: Colors.white,
                  ),
              textAlign: TextAlign.center,
            ),
          ),
        );
      },
      position: NotificationPosition.bottom,
    );
  }

  static String getErrorMessage(DioException dioError) {
    late String message;
    try {
      final Map errorMap = dioError.response!.data as Map;
      for (var key in errorMap.keys) {
        message = errorMap[key];
      }
    } catch (e) {
      message = dioError.message ?? 'Error server';
    }
    return message;
  }
}
