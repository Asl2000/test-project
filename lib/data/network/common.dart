class ApiCommon {
  static const String scheme = 'https';
  static const String host = 'jsonplaceholder.typicode.com';

  static const String apiApp = '$scheme://$host';
}
