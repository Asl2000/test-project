import 'package:dio/dio.dart';
import 'package:test_project_flutter/data/models/post/post.dart';
import 'package:test_project_flutter/data/models/user/user.dart';
import 'package:test_project_flutter/data/network/common.dart';
import 'package:test_project_flutter/data/network/exception/exception.dart';
import 'package:test_project_flutter/data/network/exception/exception_controller.dart';
import 'package:test_project_flutter/domain/services/loger.dart';
import 'package:test_project_flutter/domain/services/secure_storage_service.dart';

abstract class Api {
  //User
  Future<User> getUser({required int userId, Map<String, dynamic>? queryParameters});

  //Post
  Future<List<Post>> getListPost({Map<String, dynamic>? queryParameters});
}

class ApiRoutes {
  static const users = '/users/';
  static const posts = '/posts/';
}

class ApiDio implements Api {
  late Dio _client;
  final _timeOut = const Duration(milliseconds: 10000);

  ApiDio() {
    Dio dio = Dio(
      BaseOptions(
        baseUrl: ApiCommon.apiApp,
        connectTimeout: _timeOut,
        receiveTimeout: _timeOut,
        sendTimeout: _timeOut,
        responseType: ResponseType.json,
      ),
    );

    dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (RequestOptions options, RequestInterceptorHandler handler) async {
          logger.info(
            'REQUEST[${options.method}] => PATH: ${options.path}, QP: ${options.queryParameters}',
          );

          final token = await SecureStorageService.getToken();
          if (token != null) {
            options.headers['Authorization'] = token;
          } else {
            options.headers.remove('Authorization');
          }

          return handler.next(options);
        },
        onResponse: (Response response, ResponseInterceptorHandler handler) {
          logger.info('RESPONSE[${response.statusCode}] => PATH: ${response.requestOptions.path}');
          return handler.next(response);
        },
        onError: (DioException err, ErrorInterceptorHandler handler) {
          ExceptionController.processDioError(err);
          logger.warning('ERROR[${err.response?.statusCode}] => PATH: ${err.requestOptions.path}');
          return handler.next(err);
        },
      ),
    );

    _client = dio;
  }

  //Post

  @override
  Future<List<Post>> getListPost({Map<String, dynamic>? queryParameters}) async {
    late Response<List> response;

    try {
      response = await _client.get(
        ApiRoutes.posts,
        queryParameters: queryParameters,
      );
    } on DioException catch (e) {
      logger.warning('DioError: getListPost: $e');
      throw ServerException(message: ExceptionController.getErrorMessage(e));
    }

    try {
      final data = response.data!;
      return data.map((e) => Post.fromJson(e)).toList();
    } on TypeError catch (e) {
      logger.warning('TypeError: getListPost: $e');
      throw ServerException(message: 'Error type');
    }
  }

  //User

  @override
  Future<User> getUser({
    required int userId,
    Map<String, dynamic>? queryParameters,
  }) async {
    Response<Map<String, dynamic>> response;

    try {
      response = await _client.get(
        '${ApiRoutes.users}$userId',
        queryParameters: queryParameters,
      );
    } on DioException catch (e) {
      logger.warning('DioError: getUser: $e');
      throw ServerException(message: ExceptionController.getErrorMessage(e));
    }

    try {
      return User.fromJson(response.data!);
    } on TypeError catch (e) {
      logger.warning('TypeError: getUser: $e');
      throw ServerException(message: 'Error type');
    }
  }
}
