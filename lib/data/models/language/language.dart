import 'package:flutter/cupertino.dart';
import 'package:test_project_flutter/ui/res/assets.dart';

class Language {
  final Locale locale;
  final String name;
  final String flag;

  Language({
    required this.locale,
    required this.name,
    required this.flag,
  });

  factory Language.russian() => Language(
        locale: const Locale('ru'),
        name: 'Русский',
        flag: AppIcons.russiaFlag,
      );

  factory Language.english() => Language(
        locale: const Locale('en'),
        name: 'English',
        flag: AppIcons.usaFlag,
      );
}
