import 'package:firebase_messaging/firebase_messaging.dart';

enum NotificationType {
  information,
  link,
}

class PushNotification {
  final String title;
  final String body;
  final NotificationType type;
  final Map<String, dynamic> data;

  PushNotification({
    required this.title,
    required this.body,
    required this.type,
    required this.data,
  });

  factory PushNotification.fromRemoteMessage(RemoteMessage message) {
    NotificationType type;

    switch (message.data['type']) {
      case 'link':
        type = NotificationType.link;
        break;
      default:
        type = NotificationType.information;
    }

    return PushNotification(
      title: message.notification?.title ?? '',
      body: message.notification?.body ?? '',
      type: type,
      data: message.data,
    );
  }
}
