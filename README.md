## TEST PROJECT
Приложение с выводом постов и страницей пользователей

### Реализовано:
- архитектура приложения
- отображение списка постов
- переход на страницу пользователя поста
- обработчик ошибок при получении данных
- локализация (русский, английский)
- смена темы (светлая и темная)
- поддержка диплинков (возможность поделиться страницей пользователем)
- подключен FirebaseCrashlytics
- подключен FirebaseMessage

### Использованные библиотеки
- Api: [dio](https://pub.dev/packages/dio)
- State menager: [provider](https://pub.dev/packages/provider), [redux](https://pub.dev/packages/redux),
- Navigator 2.0: [beamer](https://pub.dev/packages/beamer)
- DI: [getIt](https://pub.dev/packages/get_it)

## Ссылки
- [API](https://jsonplaceholder.typicode.com)
- [APK](https://gitlab.com/Asl2000/test-project/-/blob/main/build/app/outputs/flutter-apk/app-release.apk) for android

----

- Version Flutter: [3.10.3](https://docs.flutter.dev/release/release-notes/release-notes-3.10.0)
- Version Dart 3.0.3